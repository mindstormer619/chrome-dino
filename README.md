# Chrome Dinosaur Game Implementation

![Chrome Dino Game](https://i.sli.mg/Y3bo2Q.gif)

This is a basic rendition of the Chrome dinosaur game. Entirely a simple frontend project based on the HTML `<canvas>` tag.

It uses simple rectangles for all the game elements.

## Possible Improvements

- Increasing difficulty by ramping up running rate (might affect the cacti spawning math)
- More obstacles (like those newfangled pterodactyls)
- More cacti! (The calculation for this can get more complicated, but it's essentially spawning multiple cacti in the space between the points where the jump starts and ends.)
- High-score tracking (*hint:* Check out `localStorage`).
- *Long shot:* Framerate-independent game speed.
